<?php

namespace App\Controllers;

use App\models\Model_auth;
use App\models\Model_register;

class Auth extends BaseController
{

    public function __construct()
    {
        helper('form');

        $this->Model_auth = new Model_auth();
    }

    public function index()
    {
        $data = array(
            'title' => 'Login',
        );
        return view('v_login', $data);
    }

    public function login()
    {
        $session = session();
        $model = new Model_register();
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');
        $data = $model->where('email', $email)->first();
        if ($data) {
            $pass = $data['password'];
            $verify_pass = password_verify($password, $pass);
            if ($verify_pass) {
                $ses_data = [
                    'email'    => $data['email'],
                    'id_user'    => $data['id_user'],
                    'password'    => $data['password'],
                    'nama_user' => $data['nama_user'],
                    'level'    => $data['level'],
                    'foto'    => $data['foto'],
                    'id_dep'    => $data['id_dep'],
                    'log'     => true,
                ];
                $session->set($ses_data);
                session()->setFlashdata('pesan', 'Login success');
                return redirect()->to(base_url('home'));
            } else {
                session()->setFlashdata('fail', 'Login Fail !!! Email or Password Error');
                return redirect()->to(base_url('auth'));
            }
        } else {
            session()->setFlashdata('errors', \Config\Services::validation()->getErrors());
            return redirect()->to(base_url('auth'));
        }
    }

    public function logout()
    {
        session()->remove('log');
        session()->remove('nama_user');
        session()->remove('id_user');
        session()->remove('email');
        session()->remove('password');
        session()->remove('level');
        session()->remove('foto');
        session()->remove('id_dep');
        session()->remove('id_kategori');
        session()->setFlashdata('logout', 'Logout success');
        return redirect()->to(base_url('auth'));
    }
}

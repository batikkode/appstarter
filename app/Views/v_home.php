<p> Hallo Selamat Datang <b><?= session()->get('nama_user') ?></b></p>
<div class="row">
    <div class="swal" data-swal="<?= session()->get('pesan'); ?>"></div>
    <?php if (session()->get('level') == 2) { ?>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-heading p-4">
                    <div class="mini-stat-icon float-right">
                        <i class="mdi mdi-file-pdf bg-danger text-white"></i>
                    </div>
                    <div>
                        <a href="<?= base_url('arsip') ?> " class="font-16">TOTAL ARSIP</a>
                    </div>
                    <h3 class="mt-4"><?= $tot_arsip ?></h3>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if (session()->get('level') == 1) { ?>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-heading p-4">
                    <div class="mini-stat-icon float-right">
                        <i class="mdi mdi-expand-all bg-primary  text-white"></i>
                    </div>
                    <div>
                        <a href="<?= base_url('kategori') ?>" class="font-16">KATEGORI</a>
                    </div>
                    <h3 class="mt-4"><?= $tot_kategori ?></h3>

                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-heading p-4">
                    <div class="mini-stat-icon float-right">
                        <i class="mdi mdi-fireplace-off bg-success text-white"></i>
                    </div>
                    <div>
                        <a href="<?= base_url('dep') ?>" class="font-16">DEPARTMENT</a>
                    </div>
                    <h3 class="mt-4"><?= $tot_dep ?></h3>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-heading p-4">
                    <div class="mini-stat-icon float-right">
                        <i class="mdi mdi-google-street-view bg-warning text-white"></i>
                    </div>
                    <div>
                        <a href="<?= base_url('user') ?>" class="font-16">TOTAL USER</a>
                    </div>
                    <h3 class="mt-4"><?= $tot_user ?></h3>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-heading p-4">
                    <div class="mini-stat-icon float-right">
                        <i class="mdi mdi-file-pdf bg-danger text-white"></i>
                    </div>
                    <div>
                        <a href="<?= base_url('arsip') ?> " class="font-16">TOTAL ARSIP</a>
                    </div>
                    <h3 class="mt-4"><?= $tot_arsip ?></h3>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
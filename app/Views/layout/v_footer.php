    </div>
    </div>
    </div>
    <footer class="footer">
        © 2019 - 2020 Develop <span class="d-none d-sm-inline-block">By <i class="mdi mdi-heart text-danger"></i>Gumilar Hasta Pratama</span>.
    </footer>
    </div>
    </div>
    <script src="<?= base_url() ?>/assets/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/metismenu.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/jquery.slimscroll.js"></script>
    <script src="<?= base_url() ?>/assets/js/waves.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/jszip.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/buttons.colVis.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>/assets/pages/datatables.init.js"></script>
    <script src="<?= base_url() ?>/assets/js/app.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/sweet-alert2/sweetalert2.all.js"></script>
    <script src="<?= base_url() ?>/assets/pages/sweet-alert.init.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/moment/moment.js"></script>
    <script src="<?= base_url() ?>/assets/plugins/x-editable/js/bootstrap-editable.min.js"></script>
    <script src="<?= base_url() ?>/assets/pages/xeditable.js"></script>
    <script>
        window.setTimeout(function() {
            $('.alert').fadeTo(250, 0).slideUp(250, function() {
                $(this).remove();
            });
        }, 3000);
    </script>
    </body>

    </html>
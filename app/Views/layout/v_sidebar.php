<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <?php if (session()->get('level') == 1) { ?>
                    <li class="menu-title">Menu Admin</li>
                <?php } ?>

                <?php if (session()->get('level') == 2) { ?>
                    <li class="menu-title">Menu User</li>
                <?php } ?>
               

                <?php if (session()->get('level') == 1) { ?>
                    <li>
                        <a href="<?= site_url('home') ?>" class="waves-effect">
                            <i class="icon-accelerator"></i> <span> Dashboard Admin </span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (session()->get('level') == 2) { ?>
                    <li>
                        <a href="<?= site_url('home') ?>" class="waves-effect">
                            <i class="icon-accelerator"></i> <span> Dashboard User </span>
                        </a>
                    </li>
                <?php } ?>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="icon-mail-open"></i><span>Arsip<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="submenu">
                        <?php if (session()->get('level') == 1) { ?>
                            <li><a href="<?= site_url('kategori') ?>">Kategori Arsip</a></li>
                            <li><a href="<?= site_url('dep') ?>">Data Departement</a></li>
                            <li><a href="<?= site_url('user') ?>">Data User</a></li>
                            <li><a href="<?= site_url('arsip') ?>">Data Arsip</a></li>
                        <?php } ?>


                        <?php if (session()->get('level') == 2) { ?>
                            <li><a href="<?= site_url('arsip') ?>">Data Arsip</a></li>
                        <?php } ?>
                    </ul>
                </li>
                <?php if (session()->get('level') == 2) { ?>
                    <li>
                        <a href="<?= site_url('profile/akun') ?>" class="waves-effect"><i class="icon-profile"></i><span>Profile</span></a>
                    </li>
                <?php } ?>
                <li>
                    <a href="<?= site_url('auth/logout') ?>"><i class="mdi mdi-power text-danger"></i><span>Logout</span></a></a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-10">
                        <h4 class="page-title"><?= $title ?></h4>
                    </div>
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="<?= site_url('home') ?>">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Pages</a></li>
                            <li class="breadcrumb-item active"><a href="<?= site_url($title) ?>"> <?= $title ?></a></li>
                        </ol>
                    </div>
                </div>
                </div>
        
  